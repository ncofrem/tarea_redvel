# TAREA REDVEL - LA BIBLIO

Código con los ejercicios propuestos hechos.

>Ejercicio 1 – nivel: básico

>* Encuentra la página de edición de un libro. Actualiza la vista para que use adecuadamente bootstrap.

> Ejercicio 2 – nivel: básico

>* Agrega la posibilidad de que un libro pueda también tener una foto de la portada. Agrégala tanto al listado en el index y donde estimes conveniente.

> Ejercicio 3 – nivel: medio

>* Modifca lo necesario para que “Editorial” sea un modelo aparte. Un libro puede tener una editorial, mientras una editorial tiene muchos libros. Por favor no pierdas datos.

> Ejercicio 4 – nivel: medio

>* Modifca lo necesario para que “Author” sea un modelo aparte. Un libro puede tener muchos autores, y un autor puede tener muchos libros. Por favor no pierdas datos.

> Ejercicio 5 – nivel: alto

>* Modifca el listado de libros: agrega un buscador (como estimes conveniente), la posibilidad de poder ordenar por las cabeceras de cada columna y paginación.

Ejecute:

```javascript
$ rake db:migrate
$ rake db:seed
```