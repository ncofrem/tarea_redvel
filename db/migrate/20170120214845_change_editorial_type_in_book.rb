class ChangeEditorialTypeInBook < ActiveRecord::Migration
  def change
  	remove_column :books, :editorial
  	add_reference :books, :editorial, index: true
  end
end
