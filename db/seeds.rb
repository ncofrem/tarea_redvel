# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

admin_user = User.find_or_create_by(:email => 'admin@example.com') do |user|
  user.password = '12345678'
end

unless Book.exists?
  now = Time.now.to_f
  long_ago = 5.years.ago.to_f
  
  def rtime(now,init_time)
    x_init_time = init_time.to_time.to_f
    Time.at(rand * (now - x_init_time) + x_init_time)
  end
  Author.create!([
    {name: "Orson Scott Card"},
    {name: "Rick Yancey"},
    {name: "Brandon Sanderson"},
    {name: "Pawan Vora"},
    {name: "Jason Beaird"},
    {name: "Jennifer Niederst Robbins"},
    {name: "Steve Krug"},
    {name: "George R. R. Martin"},
    {name: "Linda Antonsson"},
    {name: "Elio M. García"},
    {name: "J. R. R. Tolkien"},
    {name: "Christopher Tolkien"}
  ])
  Editorial.create!([
    {name: "Minotauro"},
    {name: "Ediciones B"},
    {name: "RBA"},
    {name: "Morgan Kaufmann Publishers"},
    {name: "SitePoint"},
    {name: "O'Reilly"},
    {name: "New Riders Publishing"},
    {name: "Plaza & Janés"}
  ])
  Book.create!([
    {title: "La sombra de Ender", original_title: "Ender's Shadow", translation: "Rafael Marín", edition: 1, edition_date: "2012-01-01", edition_place: "Barcelona", publication_year: 1999, isbn: "978-84-9872-591-9", cover_page_file_name: "9788498725919.jpg", cover_page_content_type: "image/jpeg", cover_page_file_size: 26807, cover_page_updated_at: "2017-01-23 14:49:32", editorial_id: 2, author_ids: [1]},
    {title: "La sombra del Hegemón", original_title: "Hegemon's Shadow", translation: "Rafael Marín", edition: 1, edition_date: "2014-02-01", edition_place: "Barcelona", publication_year: 2001, isbn: "978-84-9872-909-2", cover_page_file_name: "643568785.jpg", cover_page_content_type: "image/jpeg", cover_page_file_size: 81044, cover_page_updated_at: "2017-01-23 14:51:53", editorial_id: 2, author_ids: [1]},
    {title: "La quinta ola", original_title: "The 5th Wave", translation: "Pilar Ramírez Tello", edition: 1, edition_date: "2013-09-01", edition_place: "Barcelona", publication_year: 2013, isbn: "978-84-272-0422-5", cover_page_file_name: "9788427204225.jpg", cover_page_content_type: "image/jpeg", cover_page_file_size: 63721, cover_page_updated_at: "2017-01-20 19:49:49", editorial_id: 3, author_ids: [2]},
    {title: "El imperio final", original_title: "Mistborn", translation: "Rafael Marín Trechera", edition: 2, edition_date: "2011-06-01", edition_place: "Barcelona", publication_year: 2007, isbn: "978-84-666-3199-0", cover_page_file_name: "El_imperio_final.jpg", cover_page_content_type: "image/jpeg", cover_page_file_size: 179644, cover_page_updated_at: "2017-01-23 14:53:31", editorial_id: 2, author_ids: [3]},
    {title: "El pozo de la ascensión", original_title: "The Well of Ascension: Book Two of Mistborn", translation: "Rafael Marín Trechera", edition: 3, edition_date: "2011-06-01", edition_place: "Barcelona", publication_year: 2009, isbn: "", cover_page_file_name: "25.jpg", cover_page_content_type: "image/jpeg", cover_page_file_size: 48065, cover_page_updated_at: "2017-01-23 14:54:48", editorial_id: 2, author_ids: [3]},
    {title: "Web Application Design Patterns", original_title: "", translation: "", edition: 1, edition_date: "2017-01-21", edition_place: "Canada", publication_year: 2009, isbn: "978-0-12-374265-0", cover_page_file_name: "41j-eRH3s9L._SX403_BO1_204_203_200_.jpg", cover_page_content_type: "image/jpeg", cover_page_file_size: 28937, cover_page_updated_at: "2017-01-23 14:55:47", editorial_id: 4, author_ids: [4]},
    {title: "The principles of Beautiful Web Design", original_title: "", translation: "", edition: 2, edition_date: "2010-11-01", edition_place: "Canada", publication_year: 2010, isbn: "978-0-9805768-9-4", cover_page_file_name: "41FxC9u__VL._SX385_BO1_204_203_200_.jpg", cover_page_content_type: "image/jpeg", cover_page_file_size: 29205, cover_page_updated_at: "2017-01-23 14:56:56", editorial_id: 5, author_ids: [5]},
    {title: "Learning Web Design", original_title: "", translation: "", edition: 3, edition_date: "2007-06-01", edition_place: "", publication_year: 2001, isbn: "978-0-596-52752-5", cover_page_file_name: nil, cover_page_content_type: nil, cover_page_file_size: nil, cover_page_updated_at: nil, editorial_id: 6, author_ids: [6]},
    {title: "Don’t Make Me Think!", original_title: "", translation: "", edition: 2, edition_date: "2006-06-02", edition_place: "United States of America", publication_year: 2006, isbn: "0-321-34475-8", cover_page_file_name: nil, cover_page_content_type: nil, cover_page_file_size: nil, cover_page_updated_at: nil, editorial_id: 7, author_ids: [7]},
    {title: "Canción de hielo y fuego: Juego de tronos", original_title: "A Game of Thrones", translation: "", edition: 1, edition_date: "2002-10-01", edition_place: "España", publication_year: 1996, isbn: "978-84-96208-42-1", cover_page_file_name: "juegodetronoscanciondehieloyfuegoigrrmartin5935mlu5010436220092013o.jpg", cover_page_content_type: "image/jpeg", cover_page_file_size: 44645, cover_page_updated_at: "2017-01-23 15:02:10", editorial_id: 8, author_ids: [8]},
    {title: "Canción de hielo y fuego: Vientos de invierno", original_title: "The Winds of Winter", translation: "", edition: nil, edition_date: nil, edition_place: "", publication_year: nil, isbn: "", cover_page_file_name: "12111823.jpg", cover_page_content_type: "image/jpeg", cover_page_file_size: 35508, cover_page_updated_at: "2017-01-23 15:05:04", editorial_id: 8, author_ids: [8]},
    {title: "El mundo de hielo y fuego", original_title: "The World of Ice & Fire", translation: "", edition: nil, edition_date: nil, edition_place: "", publication_year: 2013, isbn: "", cover_page_file_name: "70256.the-world-of-ice-and-fire.jpg", cover_page_content_type: "image/jpeg", cover_page_file_size: 143767, cover_page_updated_at: "2017-01-23 15:08:34", editorial_id: 8, author_ids: [8, 9, 10]},
    {title: "El Silmarillion", original_title: "Silmarillion", translation: "", edition: 1, edition_date: "1977-02-24", edition_place: "", publication_year: 1984, isbn: "", cover_page_file_name: "silmarillion-minotauro.jpg", cover_page_content_type: "image/jpeg", cover_page_file_size: 737118, cover_page_updated_at: "2017-01-23 15:11:22", editorial_id: 1, author_ids: [11, 12]},
    {title: "El Hobbit", original_title: "The Hobbit, or There and Back Again", translation: "", edition: nil, edition_date: nil, edition_place: "", publication_year: nil, isbn: "", cover_page_file_name: "el-hobbit-j-r-r-tolkien-minotauro.jpg", cover_page_content_type: "image/jpeg", cover_page_file_size: 160949, cover_page_updated_at: "2017-01-23 15:12:42", editorial_id: 1, author_ids: [11]}
  ])
end