# == Schema Information
#
# Table name: books
#
#  id               :integer          not null, primary key
#  title            :string(255)      not null
#  editorial        :integer          not null, foreign key
#  original_title   :string(255)
#  translation      :string(255)
#  edition          :integer
#  edition_date     :date
#  edition_place    :string(255)
#  publication_year :integer
#  isbn             :string(255)
#  created_at       :datetime
#  updated_at       :datetime
#

class Book < ActiveRecord::Base

	validates :title, :editorial_id, :author_ids, :presence => true
	validates :edition, :publication_year, numericality: { only_integer: true, greater_than: 0 }, allow_nil: true

	has_attached_file :cover_page, styles: { large: "600x600>", medium: "300x300>", thumb: "100x100>" }, default_url: "/images/:style/missing.png"
	validates_attachment_content_type :cover_page, content_type: /\Aimage\/.*\z/

	belongs_to :editorial
	has_and_belongs_to_many :authors

	def self.search search
		if search
			books = joins(:editorial).includes(:authors)
					.where('title LIKE ? or edition = ? or editorials.name LIKE ?',"%#{search}%", ("#{search}").to_i, "%#{search}%")

			authors = joins(:authors).where("authors.name LIKE ?", "%#{search}%")

			Book.where(id: (authors+books).map(&:id))
		else
			all
		end
	end

end
