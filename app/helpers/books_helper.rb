module BooksHelper
	def name_authors authors

		if(authors.length == 0)
			return "-"
		end

		if(authors.length > 1)
			authors[0].name + " +#{authors.length - 1}"
		else	
			authors[0].name
		end
	end

	def sort(column, title = nil)
		title ||= column.titleize
		css_class = column == sort_column ? "current #{sort_direction}" : nil
		direction = column == sort_column && sort_direction == "asc" ? "desc" : "asc"
		link_to title, params.merge(:sort => column, :direction => direction, :page => nil), {:class => css_class}
	end
end
